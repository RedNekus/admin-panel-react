import { useRouter } from '../Router/Router'
import NProgress from 'nprogress'
import axios from 'axios'
import qs from 'qs'

NProgress.configure({
    showSpinner: false,
});

function toFormData(obj, form, namespace) {
    let fd = form || new FormData()
    let formKey

    for (let property in obj) {
        if (obj.hasOwnProperty(property)) {
            if (namespace) {
                formKey = namespace + '[' + property + ']'
            } else {
                formKey = property
            }

            if (obj[property] instanceof Date) {
                fd.append(formKey, obj[property].toISOString())
            } else if (typeof obj[property] === 'object' && obj[property] !== null && !(obj[property] instanceof File)) {
                toFormData(obj[property], fd, formKey)
            } else {
                fd.append(formKey, obj[property] === null ? '' : obj[property])
            }
        }
    }

    return fd;
}

const defaultHeaders = {
    'X-Requested-With': 'XMLHttpRequest',
}

const get = (url, data = {}) =>
    axios.get(url, { params: data, paramsSerializer: params => qs.stringify(params), headers: defaultHeaders })


const post = (url, data = {}) => axios.post(url, toFormData(data), {headers: defaultHeaders})

const api = {
    get,
    post,
}

export function useApi(redirect = true) {
    const router = useRouter()
    function responseHandler(f) {
        return async (...args) => {

            try {
                NProgress.start()
                const response = await f(...args)
                NProgress.done()
                return response.data
            } catch (e) {
                if (!e.response) {
                    throw e
                }

                NProgress.done()
                if(!redirect) {
                    return e.response.data
                }
                if (e.response.status == 303) {
                    if (e.response.data.is_new_window) {
                        window.location = e.response.data.location
                    } else {
                        router.push(e.response.data.location)
                        return e.response.data
                    }
                }
                throw e
            }
        }
    }

    return {
        get: responseHandler(api.get),
        post: responseHandler(api.post),
    }
}
