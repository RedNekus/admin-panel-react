import React, { useContext } from 'react'

const SharedDataContext = React.createContext()

export function SharedDataProvider({ data, children }) {
    return (
        <SharedDataContext.Provider value={data}>
            {children}
        </SharedDataContext.Provider>
    )
}

export function useSharedData() {
    return useContext(SharedDataContext)
}
