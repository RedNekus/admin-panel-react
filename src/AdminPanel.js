import registry from './Registry/Registry'

// Form components

// import Avatar from "./Form/Avatar"
import Boolean from './Form/Boolean'
import Checkbox from './Form/Checkbox'
import Date from './Form/Date'
import DateTime from './Form/DateTime'
import Time from './Form/Time'
import DateMonthYear from './Form/DateMonthYear'
import File from './Form/File'
import Image from './Form/Image'
import MediaImages from './Form/Media/Images'
import MediaImage from './Form/Media/Image'
import MediaFile from './Form/Media/File'
import MediaFiles from './Form/Media/Files'
import Multiple from "./Form/Multiple"
import MorphOne from "./Form/MorphOne"
import BelongsTo from "./Form/BelongsTo"
import Select from "./Form/Select"
import Text from "./Form/Text"
import Slug from "./Form/Slug"
import Color from "./Form/Color"
import TextArea from "./Form/TextArea"
import AjaxSelect from "./Form/AjaxSelect"
import Wysiwyg from "./Form/Wysiwyg"
import Password from './Form/Password'

// Pages

import HomePage from "./Pages/HomePage"
import IndexPage from "./Pages/IndexPage"
import FormPage from "./Pages/FormPage"
import LoginPage from "./Pages/LoginPage"

// Layouts

import Layout from './Layout/Main/Layout'
import BlankLayout  from './Layout/Blank/Layout'

// registry.registerComponent('form/avatar', Avatar)
registry.registerComponent('form/boolean', Boolean)
registry.registerComponent('form/checkbox', Checkbox)
registry.registerComponent('form/date', Date)
registry.registerComponent('form/time', Time)
registry.registerComponent('form/dateTime', DateTime)
registry.registerComponent('form/dateMonthYear', DateMonthYear)
registry.registerComponent('form/file', File)
registry.registerComponent('form/image', Image)
registry.registerComponent('form/mediaImages', MediaImages)
registry.registerComponent('form/mediaImage', MediaImage)
registry.registerComponent('form/mediaFiles', MediaFiles)
registry.registerComponent('form/mediaFile', MediaFile)
registry.registerComponent('form/multiple', Multiple)
registry.registerComponent('form/belongsTo', BelongsTo)
registry.registerComponent('form/morphOne', MorphOne)
registry.registerComponent('form/select', Select)
registry.registerComponent('form/text', Text)
registry.registerComponent('form/slug', Slug)
registry.registerComponent('form/color', Color)
registry.registerComponent('form/password', Password)
registry.registerComponent('form/textArea', TextArea)
registry.registerComponent('form/ajaxSelect', AjaxSelect)
registry.registerComponent('form/wysiwyg', Wysiwyg)

registry.registerComponent('page/IndexPage', IndexPage)
registry.registerComponent('page/HomePage', HomePage)
registry.registerComponent('page/FormPage', FormPage)
registry.registerComponent('page/LoginPage', LoginPage)

registry.registerComponent('layout/Layout', Layout)
registry.registerComponent('layout/BlankLayout', BlankLayout)
