import React from 'react'
import LayoutWrapper from './LayoutWrapper'

export default function Layout ({ children }) {
    return (
        <LayoutWrapper>
            {children}
        </LayoutWrapper>
    )
}
