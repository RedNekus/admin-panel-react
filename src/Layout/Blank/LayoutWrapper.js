import React from 'react'
import { Layout } from 'antd'
const { Content } = Layout

export default function LayoutWrapper(props) {
    const {
        children,
    } = props

    return (
        <Layout className='h-100'>
            <Layout>
                <Content>
                    {children}
                </Content>
            </Layout>
        </Layout>
    )
}
