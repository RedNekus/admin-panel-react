import React from 'react'

export default function LayoutFooter(props) {

    const {
        copyright
    } = props

    return (
        <div className='copyright'>
            { copyright }
        </div>
    )
}
