import React from 'react'
import { Avatar, Col, Dropdown, Icon, Menu, Row, Button } from 'antd'
import { useSharedData } from '../../SharedData/SharedData'
import { Link, useRouter } from '../../Router/Router'
import { useApi } from '../../Api/Api'

export default function LayoutHeader (props) {
    const {
        text,
        siderCollapseState,
        onSiderButtonClick,
    } = props

    const router = useRouter()
    const api = useApi()
    const sharedData = useSharedData()

    async function onLogout(e) {
        e.preventDefault()
        await api.post('/admin/logout')
        router.push('/admin/login')
    }

    const menu = (
        <Menu>
            <Menu.Item>
                <Link to={`/admin/users/${sharedData.user.id}/edit`}>
                    <Icon type='edit'/> Редактировать
                </Link>
            </Menu.Item>
            <Menu.Item>
                <Link callback={onLogout}>
                    <Icon type='logout'/> Выход
                </Link>
            </Menu.Item>
        </Menu>
    )

    return (
        <Row gutter={[8, 8]} type='flex' justify='space-between'>
            <Col>
                <Icon
                    className='trigger'
                    type={siderCollapseState ? 'menu-unfold' : 'menu-fold'}
                    onClick={onSiderButtonClick}
                />
            </Col>
            <Col>
                <Row type='flex' justify='space-around'>
                    <Col>
                        <Dropdown overlay={menu} placement='bottomLeft'>
                            <div>
                                <Avatar icon='user'
                                        style={{ marginRight: '6px' }}/>
                                {sharedData.user.first_name}
                            </div>
                        </Dropdown>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}
