import React from 'react'
import LayoutWrapper from './LayoutWrapper'
import { useSharedData } from "../../SharedData/SharedData"

export default function Layout ({ children, menu }) {
    const { config } = useSharedData()

    return (
        <LayoutWrapper
            headerProps={{
                text: config.title,
            }}
            navProps={{
                navBarLinks: menu,
                logo: {
                    src: config.logo.white,
                    href: '/admin',
                    alt: config.title,
                },
                sider: {
                    width: config.sider && config.sider.width ? config.sider.width : 200,
                },
            }}
            footerProps={{
                copyright: (
                    <>
                        Разработка - <a
                            href={config.copyright_url}
                            target='_blank'
                        >Веб Секрет
                        </a>
                    </>
                ),
            }}
        >
            {children}
        </LayoutWrapper>
    )
}
