import React from 'react'
import { Layout } from 'antd'
import LayoutHeader from './LayoutHeader'
import LayoutNav from './LayoutNav'
import LayoutFooter from './LayoutFooter'

const { Header, Content, Sider, Footer } = Layout

export default function LayoutWrapper(props) {
    const [siderCollapseState, setSiderCollapseState] = React.useState(true)

    function handleCollapseButton () {
        setSiderCollapseState(!siderCollapseState)
    }

    const {
        headerProps,
        navProps,
        footerProps,
        children,
    } = props

    const headerPropsWithSiderHandler = {
        ...headerProps,
        siderCollapseState: siderCollapseState,
        onSiderButtonClick: handleCollapseButton,
    }
    const header = React.createElement(LayoutHeader, headerPropsWithSiderHandler)
    const nav = React.createElement(LayoutNav, navProps)
    const footer = React.createElement(LayoutFooter, footerProps)

    return (
        <Layout className='h-100'>
            <Sider
                width={navProps.sider.width}
                trigger={null}
                collapsed={siderCollapseState}
                onCollapse={handleCollapseButton}
                style={{
                    overflow: 'auto',
                    height: '100vh',
                    position: 'fixed',
                    left: 0,
                }}
            >
                {nav}
            </Sider>
            <Layout style={{ marginLeft: siderCollapseState ? 80 : navProps.sider.width }}>
                <Header>
                    {header}
                </Header>
                <Content className='ant-layout-content__full'>
                    {children}
                </Content>
                <Footer>
                    {footer}
                </Footer>
            </Layout>
        </Layout>
    )
}
