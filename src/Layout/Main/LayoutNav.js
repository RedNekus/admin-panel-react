import React, { useEffect, useState } from 'react'
import { Icon, Menu, Badge } from 'antd'
import { Link } from '../../Router/Router'

const { SubMenu } = Menu

export default function LayoutNav (props) {
    const {
        logo,
        navBarLinks,
    } = props

    const [selectedLinks, setSelectedLinks] = useState([location.pathname])

    useEffect(() => {
        menuResolver(navBarLinks)
    }, [location.pathname])

    function menuResolver (links, parents = [], level = 0) {
        links.forEach(link => {
            if (link.to === location.pathname) {
                setSelectedLinks([link.to])
            }

            if (link.children && link.children.length > 0) {
                return menuResolver(link.children)
            }
        })
    }

    function renderItem (item, index) {
        if (!item.children) {
            return (
                <Menu.Item
                    key={item.to}
                >
                    <Link to={item.to}>
                        <Icon type={item.icon}/>
                        <span>{item.value} </span>
                        {item.badge && <div className="badge-green">{item.badge}</div>}
                    </Link>
                </Menu.Item>
            )
        }

        return (
            <SubMenu
                key={index}
                title={
                    <span>
                        <Icon type={item.icon}/>
                        <span>{item.value}</span>
                        {item.badge && <div className="badge-green">{item.badge}</div>}
                    </span>
                }
            >
                {item.children.map((item, index) => renderItem(item, index))}
            </SubMenu>
        )
    }

    return (
        <>
            <div className='ant-layout-sider__logo'>
                <Link
                    to={logo.href}
                >
                    <img
                        src={logo.src}
                        alt={logo.alt}
                    />
                </Link>
            </div>
            <Menu mode='inline'
                  theme='dark'
                  defaultSelectedKeys={selectedLinks}
            >
                {navBarLinks.map((item, index) => renderItem(item, index))}
            </Menu>
        </>
    )
}
