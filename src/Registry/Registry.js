import React, { useContext } from 'react'

export const RegistryContext = React.createContext(null)

const registry = new Map()

function registerComponent(name, Component) {
    registry.set(name, Component)
}

function find(name, defaultValue) {
    return registry.get(name) || defaultValue
}

export function RegistryProvider({ children }) {
    return (
        <RegistryContext.Provider value={{ find }}>
            {children}
        </RegistryContext.Provider>
    )
}

export function useRegistry() {
    return useContext(RegistryContext)
}

export default {
    registerComponent,
    find,
}
