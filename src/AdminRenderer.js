import { RegistryProvider, useRegistry } from './Registry/Registry'
import { SharedDataProvider } from "./SharedData/SharedData"
import React, { useEffect, useRef, useState } from 'react'
import Router, { useRouter } from './Router/Router'
import ruRU from 'antd/es/locale/ru_RU'
import { ConfigProvider } from 'antd'
import { useApi } from './Api/Api'
import './AdminPanel'

function AdminRenderer ({ component, props, layout, layoutProps, shared }) {
    const initial = useRef(true)
    const [page, setPage] = useState({
        component,
        props,
        layout,
        shared,
        layoutProps,
    })
    const api = useApi()
    const registry = useRegistry()
    const router = useRouter()

    useEffect(() => {
        if (initial.current) {
            initial.current = false
            return
        }

        async function fetch () {
            const response = await api.get(router.location)

            setPage({
                component: response.component,
                props: response.props,
                layout: response.layout,
                shared: response.shared,
                layoutProps: Object.assign(layoutProps, {
                    menu : response.menu
                }),
            })
        }

        fetch()
    }, [router.location])

    const Component = registry.find(`page/${page.component}`)
    const Layout = registry.find(`layout/${page.layout}`)

    return (
        <SharedDataProvider data={page.shared}>
            <ConfigProvider locale={ruRU}>
                <Layout {...layoutProps}>
                    <Component {...page.props} />
                </Layout>
            </ConfigProvider>
        </SharedDataProvider>
    )
}

export default function AdminRendererWrapper ({ children, ...props }) {
    return (
        <RegistryProvider>
            <Router>
                <AdminRenderer {...props}>
                    {children}
                </AdminRenderer>
            </Router>
        </RegistryProvider>
    )
}
