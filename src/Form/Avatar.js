import React, { useRef, useState, useEffect } from 'react'

export default observer(function Avatar({ value, onChange, label, error }) {
    const fileRef = useRef(null)
    const [src, setSrc] = useState(null)

    useEffect(() => {
        if (value instanceof File) {
            const reader = new FileReader()

            reader.onload = e => setSrc(e.target.result)

            reader.readAsDataURL(value)
        } else {
            setSrc(null)
        }
    }, [value])

    return (
        <Form.Group label={label}>
            {(value instanceof File || typeof value == 'string') &&
                <>
                    <div className="mb-2">
                        <TablerAvatar size="xxl" imageURL={value instanceof File ? src : value} />
                    </div>
                    <Button
                        className="mr-2"
                        color="info"
                        size="sm"
                        icon="edit"
                        type="button"
                        onClick={(e) => (e.preventDefault(), fileRef.current.click())}
                    >
                        Изменить
                    </Button>
                    <Button
                        color="danger"
                        size="sm"
                        icon="trash"
                        onClick={() => onChange(null)}
                    >
                        Удалить
                    </Button>
                </>
            }
            {!value &&
                <Button
                    color="success"
                    size="sm"
                    icon="download"
                    type="button"
                    onClick={(e) => (e.preventDefault(), fileRef.current.click())}
                >
                    Загрузить
                </Button>
            }
            <input
                style={{ display: 'none' }}
                ref={fileRef}
                type="file"
                onChange={e => onChange(e.target.files[0])}
            />
        </Form.Group>
    )
})
