import TinyMCE from "./Wysiwyg/TinyMCE"
import Quill from "./Wysiwyg/Quill"
import React from 'react'

export default function Wysiwyg({ driver, ...props }) {
    switch (driver) {
        case 'quill':
            return <Quill {...props} />
        case 'tinymce':
            return <TinyMCE {...props} />
    }
}
