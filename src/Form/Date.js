import { Input, DatePicker } from 'antd'
import moment from 'moment'
import React from 'react'

export default function Date({ label, value, onChange, readonly }) {
    if (readonly) {
        return (
            <Input
                label={label}
                placeholder={label}
                value={moment(value).format('YYYY-MM-DD')}
                onChange={e => onChange(e.target.value)}
                readOnly
            />
        )
    }

    return (
        <DatePicker
            value={value && moment(value)}
            onChange={value => !readonly && onChange(value ? value.toDate() : null)}
            disabled={readonly}
        />
    )
}
