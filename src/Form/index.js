import HasMany from './Multiple'
import Boolean from './Boolean'
import Avatar from './Avatar'
import Select from './Select'
import Date from './Date'
import DateTime from './DateTime'
import File from './File'
import Text from './Text'
import TextArea from './TextArea'
import Wysiwyg from './Wysiwyg'
import AjaxSelect from './AjaxSelect'

export default {
    HasMany,
    Boolean,
    Avatar,
    Select,
    Date,
    DateTime,
    File,
    Text,
    TextArea,
    Wysiwyg,
    AjaxSelect,
}
