import React from 'react'

export default function File({ error, label, onChange, value }) {
    return (
        <Form.FileInput
            invalid={!!error}
            feedback={error}
            label={value || label}
            placeholder={value || label}
            onChange={e => onChange(e.target.files[0])}
        />
    )
}
