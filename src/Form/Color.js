import { Input } from "antd"
import React from 'react'

export default function Text({ value, onChange, label, readonly, placeholder }) {
    return (
        <Input
            type='color'
            label={label}
            placeholder={placeholder || label}
            value={value}
            onChange={e => onChange(e.target.value)}
            readOnly={readonly}
        />
    )
}
