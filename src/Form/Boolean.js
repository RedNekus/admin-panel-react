import { Radio } from 'antd'
import React from 'react'

export default function Boolean({ value, onChange }) {
    function handleChange(newValue) {
        onChange(newValue == value ? null : newValue)
    }

    const mappedValue = value === null
        ? null
        : value === true ? '1' : '0'

    return (
        <Radio.Group
            value={mappedValue}
            buttonStyle="solid"
        >
            <Radio.Button value="1" onClick={() => handleChange(true)}>Да</Radio.Button>
            <Radio.Button value="0" onClick={() => handleChange(false)}>Нет</Radio.Button>
        </Radio.Group>
    )
}
