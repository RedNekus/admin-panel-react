import React, { useState, useRef, useEffect } from 'react'
import 'react-quill/dist/quill.snow.css'
import ReactQuill from 'react-quill'
import { useApi } from '../../Api/Api'

const defaultToolbar = [
    [{ 'header': [2, 3, false] }],
    ['bold', 'italic', 'underline','strike', 'blockquote'],
    [{'list': 'bullet'}],
    ['link', 'image'],
]

export default function Quill({ value, onChange, label, imageUploadUrl, toolbar = defaultToolbar }) {
    const [mounted, setMounted] = useState(false)
    const api = useApi()
    const quill = useRef(null)
    const boundsElement = useRef(null)

    // avoids rendering without boundsElement
    useEffect(() => {
        setMounted(true)
    }, [])

    const defaultModules = useRef({
        toolbar: {
            container: toolbar,
            handlers: {
                image: async () => {
                    const fileHolder = document.createElement("input")
                    fileHolder.setAttribute("type", "file")
                    fileHolder.setAttribute('accept', 'image/*')
                    fileHolder.onchange = async () => {
                        const response = await api.post(imageUploadUrl, { file: fileHolder.files[0] })
                        const range = quill.current.getEditor().getSelection()
                        quill.current.getEditor().insertEmbed(range.index, 'image', response.url, "user")
                        fileHolder.remove()
                    }
                    fileHolder.click()
                }
            },
        },
    })

    return (
        <div ref={boundsElement}>
            {mounted &&
                <ReactQuill
                    ref={quill}
                    bounds={boundsElement.current}
                    placeholder={label}
                    onChange={onChange}
                    value={value}
                    modules={defaultModules.current}
                />
            }
        </div>
    )
}
