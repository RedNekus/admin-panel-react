import { Editor } from '@tinymce/tinymce-react'
import { useApi } from "../../Api/Api"
import React from 'react'

export default function TinyMCE({ value, onChange, label, imageUploadUrl, toolbar }) {
    const api = useApi()

    async function handleImageUpload(blobInfo, success) {
        const response = await api.post(imageUploadUrl, { file: blobInfo.blob() })
        success(response.url)
    }

    return (
        <Editor
            value={value}
            onEditorChange={onChange}
            plugins="image table link autolink paste autoresize advlist lists code"
            toolbar={toolbar}
            init={{
                valid_children : '+body[style]',
                advlist_bullet_styles: "default",
                automatic_uploads: true,
                images_upload_url: imageUploadUrl,
                paste_as_text: true,
                images_upload_handler: handleImageUpload,
                content_style: ' .embedly-card { background: #b0ffd6; }',
                style_formats: [
                    { title: 'Embedly', selector: 'a', classes: 'embedly-card', attributes: { 'data-card-controls': '0' } },
                    { title: 'Embedly Full Width', selector: 'a', classes: 'embedly-card', attributes: { 'data-card-controls': '0', 'data-card-width': '100%' } },
                ],
            }}
        />
    )
}
