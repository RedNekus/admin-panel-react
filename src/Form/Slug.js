import { Input, Icon } from "antd"
import { green, red } from '@ant-design/colors'
import React, { useEffect, useState } from 'react'
import { useValues } from './Form'
import slugify from 'slugify'

export default function Slug({ value, onChange, label, disabled, placeholder, fromField }) {
    const fromFieldValue = useValues().values[`${fromField}`]||null
    const [isLocked, setIsLocked] = useState(disabled)

    useEffect(() => {
        if (isLocked) {
            return;
        }

        if (fromFieldValue) {
            onChange(slugify(fromFieldValue, {
                lower: true,
                replacement: '-',
            }))
            return;
        }
        onChange(null)

    },[fromFieldValue])

    function handleLock () {
        setIsLocked(!isLocked)
    }

    return (
        <Input
            label={label}
            placeholder={placeholder || label}
            value={value}
            onChange={e => onChange(e.target.value)}
            readOnly={isLocked}
            suffix={
                <Icon theme='twoTone' twoToneColor={isLocked ? green.primary : red.primary} type={isLocked ? 'lock' : 'unlock'} onClick={handleLock}/>
            }
        />
    )
}
