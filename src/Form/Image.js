import React, {useEffect, useState} from 'react'
import { Upload } from 'antd'

export default function Image({ value, onChange, label, error }) {
    const [src, setSrc] = useState(null)

    useEffect(() => {
        if (value instanceof File) {
            const reader = new FileReader()

            reader.onload = e => setSrc(e.target.result)

            reader.readAsDataURL(value)
        } else {
            setSrc(null)
        }
    }, [value])

    function onRemove() {
        onChange(null)
    }

    return (
        <Upload
            name="avatar"
            listType="picture-card"
            className="avatar-uploader"
            showUploadList={true}
            onRemove={onRemove}
            fileList={(value instanceof File || (value && typeof value == 'string'))
                ? (
                    [{
                        uid: 1,
                        url: value instanceof File ? src : value,
                    }]
                )
                : []
            }
            beforeUpload={file => {
                onChange(file)
                return false
            }}
        >
            <div>
                <div className="ant-upload-text">Загрузить</div>
            </div>
        </Upload>
    )
}
