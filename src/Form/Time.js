import { Input, TimePicker } from 'antd'
import moment from 'moment'
import React from 'react'

export default function Time({ label, value, onChange, momentFormat, readonly }) {
    if (readonly) {
        return (
            <Input
                label={label}
                placeholder={label}
                value={moment(value).format(momentFormat)}
                onChange={e => onChange(e.target.value)}
                readOnly
            />
        )
    }
    return (
        <TimePicker
            value={value && moment(value, momentFormat)}
            format={momentFormat}
            onChange={value => !readonly && onChange(value ? value.format('HH:mm') : null)}
            disabled={readonly}
        />
    )
}
