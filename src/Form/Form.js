import React, { useContext, useEffect, useMemo, useState } from 'react'
import { useRegistry } from '../Registry/Registry'
import Text from './Text'
import { Form as AntdForm, notification } from 'antd'

function unflatten(data) {
    let result = {}
    for (let i in data) {
        let keys = i.split('.')
        keys.reduce(function(r, e, j) {
            return r[e] || (r[e] = isNaN(Number(keys[j + 1])) ? (keys.length - 1 === j ? data[i] : {}) : [])
        }, result)
    }
    return result
}

const ValuesContext = React.createContext(null)

export function ValuesProvider({ children, values, onChange }) {
    function setValue(name, value) {
        onChange({ ...values, [name]: value })
    }

    return (
        <ValuesContext.Provider value={{ values, onChange, setValue }}>
            {children}
        </ValuesContext.Provider>
    )
}

export function useValues() {
    return useContext(ValuesContext)
}

const ErrorsContext = React.createContext(null)

export function ErrorsProvider({ children, errors, findError }) {
    return (
        <ErrorsContext.Provider value={{ errors, findError }}>
            {children}
        </ErrorsContext.Provider>
    )
}

export function useErrors() {
    return useContext(ErrorsContext)
}

export function FormProvider({ values, onChange, errors, findError, children }) {
    return (
        <ErrorsProvider errors={errors} findError={findError}>
            <ValuesProvider values={values} onChange={onChange}>
                {children}
            </ValuesProvider>
        </ErrorsProvider>
    )
}

export function Form({ values, onChange, errors, findError, children, onSubmit, className, isRoot = true }) {
    return (
        <FormProvider values={values} onChange={onChange} errors={errors} findError={findError}>
            {isRoot
                ?
                <AntdForm className={className} onSubmit={onSubmit}>
                    {children}
                </AntdForm>
                :
                <div className={className} onSubmit={onSubmit}>
                    {children}
                </div>
            }
        </FormProvider>
    )
}

export function useFields(fields, initialValues = {}) {
    const fieldsDefaultValues = useMemo(() =>
            fields.reduce((acc, field) => {
                if (field.type == 'date') {
                    acc[field.attribute] = field.value ? new Date(field.value) : null
                } else {
                    acc[field.attribute] = field.value
                }

                return acc
            }, {})
        , [fields])

    const defaultValues = useMemo(() =>
            fields.reduce((acc, field) => {
                acc[field.attribute] = initialValues[field.attribute] || fieldsDefaultValues[field.attribute]
                return acc
            }, {})
        , [fields])

    const [values, setValues] = useState(defaultValues)

    const fieldsAsElements = useMemo(() =>
            fields.map((field, idx) =>
                <Field
                    key={idx + field.attribute}
                    type={field.type}
                    name={field.attribute}
                    label={field.label}
                    readonly={field.readonly}
                    placeholder={field.placeholder}
                    {...field.meta}
                />
            )
        , [fields])

    const mappedFields = useMemo(() =>
            fieldsAsElements.reduce((acc, field) =>
                    (acc[field.props.name] = field, acc)
                , fieldsAsElements)
        , [fieldsAsElements])

    return {
        values,
        setValues,
        elements: mappedFields,
        defaultValues: fieldsDefaultValues,
        get filledValues() {
            return Object.keys(values).reduce((acc, key) => {
                if (values[key] !== fieldsDefaultValues[key]) {
                    acc[key] = values[key]
                }

                return acc
            }, {})
        },
        remove(key) {
            const newValues = {
                ...values,
                [key]: fieldsDefaultValues[key],
            }

            setValues(newValues)

            return newValues
        },
        clear() {
            setValues(fieldsDefaultValues)
            return fieldsDefaultValues
        },
    }
}

function usePromise(f) {
    const [state, setState] = useState({ state: null })

    return [
        state,
        async (...args) => {
            try {
                setState({ state: 'pending' })
                const result = await f(...args)
                setState({ state: 'resolved', result })
            } catch (exception) {
                setState({ state: 'rejected', exception })
            }
        }
    ]
}

export function FieldsForm({ fields: fieldsList, children, onSubmit: onSubmitHandler, values: controlledValues, onChange: controlledOnChange }) {
    const fields = useFields(fieldsList)
    const values = controlledValues || fields.values
    const onChange = controlledOnChange || fields.setValues
    const [submission, onSubmit] = usePromise(async function(e) {
        e.preventDefault()
        await onSubmitHandler(values)
    })
    const isSubmitting = submission.state == 'pending'
    const errors = submission.state == 'rejected'
        ? submission.exception.response.data.errors || null
        : null

    useEffect(() => {
        if (submission.state == 'rejected' && submission.exception.response.data.message) {
            notification.error({ message: submission.exception.response.data.message })
        }
    }, [submission.state])

    function findError(errors, name) {
        return errors && errors[name] && errors[name][0]
    }

    return (
        <FieldsForm.Controlled
            fields={fieldsList}
            values={values}
            onChange={onChange}
            errors={errors}
            findError={findError}
            onSubmit={onSubmit}
            isSubmitting={isSubmitting}
        >
            {children({
                fields,
                values,
                onSubmit,
                isSubmitting,
            })}
        </FieldsForm.Controlled>
    )
}

FieldsForm.Controlled = function FieldsFormControlled({ values, onChange, errors, findError, children, onSubmit }) {
    if (!values) {
        return null
    }

    return (
        <Form values={values} onChange={onChange} errors={errors} findError={findError} onSubmit={onSubmit}>
            {children}
        </Form>
    )
}

export function Field({ name, type, ...props }) {
    const { values, setValue } = useValues()
    const { errors, findError } = useErrors()
    const registry = useRegistry()
    const Component = registry.find(`form/${type}`, Text)

    return (
        <AntdForm.Item help={findError(errors, name)} validateStatus={findError(errors, name) ? 'error' : null} label={props.label}>
            <Component
                value={values[name]}
                name={name}
                onChange={value => setValue(name, value)}
                {...props}
            />
        </AntdForm.Item>
    )
}
