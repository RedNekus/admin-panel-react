import React from 'react'
import { Select as AntdSelect } from 'antd'

const possibleNumberToNumber = numberOrString =>
    isNaN(numberOrString) ? numberOrString : Number(numberOrString)

export default function Select({ value, onChange, label, readonly, placeholder : initialPlaceholder, options, multiple = false, tags = false }) {
    const placeholder = initialPlaceholder ? initialPlaceholder : '- Выберите значение -'
    const mode = multiple ? 'multiple' : tags ? 'tags' : 'default'

    if (value) {
        value = Array.isArray(value)
            ? value.map(possibleNumberToNumber)
            : possibleNumberToNumber(value)
    }

    function renderOption(option, idx) {
        if (option.is_group) {
            return <AntdSelect.OptGroup
                key={idx}
                label={option.label}
            >
                {option.options.map((option, childIdx) =>
                    renderOption(option, childIdx)
                )}
            </AntdSelect.OptGroup>
        }

        return <AntdSelect.Option
            key={idx}
            value={option.value}
            title={option.label}
        >
            {option.label}
        </AntdSelect.Option>
    }

    return (
        <AntdSelect
            mode={mode}
            showSearch
            value={value||undefined}
            placeholder={placeholder}
            disabled={readonly}
            onChange={onChange}
            label={label}
            filterOption={true}
            optionFilterProp={'title'}
            defaultActiveFirstOption={mode === 'tags'}
        >
            {options.map((option, idx) =>
                renderOption(option, idx)
            )}
        </AntdSelect>
    )
}
