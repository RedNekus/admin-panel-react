import { DatePicker, Input } from 'antd'
import moment from 'moment'
import React from 'react'

export default function DateMonthYear({ label, value, onChange, readonly }) {
    if (readonly) {
        return (
            <Input
                label={label}
                placeholder={label}
                value={moment(value).format('MMMM YYYY')}
                onChange={e => onChange(e.target.value)}
                readOnly
            />
        )
    }

    return (
        <DatePicker.MonthPicker
            label={label}
            placeholder={label}
            value={value ? moment(value) : null}
            format='MMMM YYYY'
            disabled={readonly}
            onChange={value => !readonly && onChange(value ? value.toDate() : null)}
        />
    )
}
