import React, { useState } from 'react'
import { Upload, Modal } from 'antd'

export default function MediaImage ({ value, onChange, label, error }) {
    const [previewVisible, setPreviewVisible] = useState(false)
    const [previewImage, setPreviewImage] = useState({})

    function onBeforeUpload (file) {
        handleChanges(file)
        return false
    }

    async function handleChanges (file) {
        if (file instanceof File) {
            file.url = await getBase64(file)
        }
        onChange(file)
    }

    function getBase64 (file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onload = () => resolve(reader.result)
            reader.onerror = error => reject(error)
        })
    }

    function onRemove (file) {
        onChange(null)
    }

    function onPreview(file) {
        setPreviewImage(file)
        setPreviewVisible(!previewVisible)
    }

    function onPreviewClose () {
        setPreviewVisible(!previewVisible)
        setPreviewImage({})
    }

    return (
        <>
            <Upload
                listType="picture-card"
                showUploadList={true}
                fileList={value ? [value] : []}
                onRemove={onRemove}
                onPreview={onPreview}
                beforeUpload={file => onBeforeUpload(file)}
            >
                <div>
                    <div className="ant-upload-text">Загрузить</div>
                </div>
            </Upload>
            <Modal visible={previewVisible}
                   centered
                   title={previewImage.name || 'Превью'}
                   footer={null}
                   onCancel={onPreviewClose}
            >
                <img
                    alt={previewImage.name || 'Превью'}
                    style={{
                        width: '100%',
                        opacity: previewVisible ? 1 : 0,
                    }}
                    src={previewImage.lg_preview || previewImage.url}
                />
            </Modal>
        </>
    )
}
