import React, { useState } from 'react'
import { Upload, Button, Icon } from 'antd'

export default function MediaFile ({ value, onChange, label, error }) {
    function onBeforeUpload (file) {
        handleChanges(file)
        return false
    }

    async function handleChanges (file) {
        onChange(file)
    }

    function onRemove (file) {
        onChange(null)
    }

    return (
        <>
            <Upload
                showUploadList={true}
                fileList={value ? [value] : []}
                onRemove={onRemove}
                beforeUpload={file => onBeforeUpload(file)}
            >
                <Button>
                    <Icon type="upload" /> Загрузить
                </Button>
            </Upload>
        </>
    )
}
