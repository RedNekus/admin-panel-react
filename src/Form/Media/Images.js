import React, { useState, useEffect } from 'react'
import { Upload, Modal } from 'antd'

export default function MediaImages ({ value, onChange, label, error, limit }) {
    const [previewVisible, setPreviewVisible] = useState(false)
    const [previewImage, setPreviewImage] = useState({})
    const [dirtyImages, setDirtyImages] = useState([])

    useEffect(() => {
        if (dirtyImages.length) {
            handleDirtyImages()
        }
    }, [dirtyImages])

    async function handleDirtyImages() {
        let newValue = [...value]
        for (const dirtyImage of dirtyImages) {
            if (dirtyImage instanceof File) {
                dirtyImage.url = await getBase64(dirtyImage)
            }

            newValue = [...newValue, dirtyImage]
        }

        if (limit && newValue.length > limit) {
            newValue = newValue.slice(-limit)
        }

        onChange(newValue)
        setDirtyImages([])
    }

    function addDirtyImage(image) {
        setDirtyImages(dirtyImages => {
            return [...dirtyImages, image]
        })
    }

    function onBeforeUpload (file) {
        addDirtyImage(file)
        return false
    }

    function getBase64 (file) {
        return new Promise((resolve, reject) => {
            const reader = new FileReader()
            reader.readAsDataURL(file)
            reader.onload = () => resolve(reader.result)
            reader.onerror = error => reject(error)
        })
    }

    function onRemove (file) {
        const removedFileIndex = value.indexOf(file)
        const newFilesList = value.slice()
        newFilesList.splice(removedFileIndex, 1)

        onChange(newFilesList)
    }

    function onPreview(file) {
        setPreviewImage(file)
        setPreviewVisible(!previewVisible)
    }

    function onPreviewClose () {
        setPreviewVisible(!previewVisible)
        setPreviewImage({})
    }

    return (
        <>
            <Upload
                multiple
                listType="picture-card"
                showUploadList={true}
                fileList={value}
                onRemove={onRemove}
                onPreview={onPreview}
                beforeUpload={onBeforeUpload}
            >
                <div>
                    <div className="ant-upload-text">Загрузить</div>
                </div>
            </Upload>
            <Modal visible={previewVisible}
                   centered
                   title={previewImage.name || 'Превью'}
                   footer={null}
                   onCancel={onPreviewClose}
            >
                <img
                    alt={previewImage.name || 'Превью'}
                    style={{
                        width: '100%',
                        opacity: previewVisible ? 1 : 0,
                    }}
                    src={previewImage.lg_preview || previewImage.url}
                />
            </Modal>
        </>
    )
}
