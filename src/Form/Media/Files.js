import React, { useState, useEffect } from 'react'
import { Upload, Button, Icon } from 'antd'

export default function MediaFiles ({ value, onChange, label, error, limit }) {
    const [dirtyFiles, setDirtyFiles] = useState([])

    useEffect(() => {
        if (dirtyFiles.length) {
            handleDirtyFiles()
        }
    }, [dirtyFiles])

    async function handleDirtyFiles() {
        let newValue = [...value]
        for (const dirtyFile of dirtyFiles) {
            newValue = [...newValue, dirtyFile]
        }

        if (limit && newValue.length > limit) {
            newValue = newValue.slice(-limit)
        }

        onChange(newValue)
        setDirtyFiles([])
    }

    function addDirtyFile(file) {
        setDirtyFiles(dirtyFiles => {
            return [...dirtyFiles, file]
        })
    }

    function onBeforeUpload (file) {
        addDirtyFile(file)
        return false
    }

    function onRemove (file) {
        const removedFileIndex = value.indexOf(file)
        const newFilesList = value.slice()
        newFilesList.splice(removedFileIndex, 1)

        onChange(newFilesList)
    }

    return (
        <>
            <Upload
                multiple
                showUploadList={true}
                fileList={value}
                onRemove={onRemove}
                beforeUpload={onBeforeUpload}
            >
                <Button>
                    <Icon type="upload" /> Загрузить
                </Button>
            </Upload>
        </>
    )
}
