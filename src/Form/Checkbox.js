import { Checkbox as AntdCheckbox } from 'antd'
import React from 'react'

export default function Checkbox({ value, onChange }) {
    return (
        <AntdCheckbox
            checked={value}
            onChange={e => onChange(e.target.checked)}
        >
        </AntdCheckbox>
    )
}
