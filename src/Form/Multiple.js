import { Form as AntdForm, Button, Card, Row, Col } from 'antd'
import { Field, Form, useErrors } from "./Form"
import React, {useRef} from 'react'

export default function Multiple({ error, value, name, label, onChange, fields, defaultFields, canManipulate, limit, horizontal = true }) {
    const parentErrors = useErrors()

    function renderChildField(childField, idx, fieldIdx) {
        // FIXME: it doesn't work when we delete and create row again
        childField = (fields[idx] && fields[idx][fieldIdx]) || childField
        return (
            <Field
                key={childField.type + fieldIdx}
                name={childField.attribute}
                type={childField.type}
                label={childField.label}
                isRequired={childField.isRequired}
                readonly={childField.readonly}
                {...childField.meta}
            />
        )
    }

    const wrap = (children, idx) => {
        return horizontal
            ? (
                <div key={`horizontal-${idx}`}>
                    {children}
                </div>
            )
            : (
                <Card
                    key={`vertical-${idx}`}
                    title={`#${idx}`}
                    extra={canManipulate && <Button type="danger" icon="delete" onClick={handleRemove(idx)}>Удалить</Button>}
                >
                    {children}
                </Card>
            )
    }

    function handleAdd() {
        if (limit && value.length >= limit) {
            return;
        }

        onChange([
            ...value,
            defaultFields.reduce((acc, cur) => {
                acc[cur.attribute] = cur.defaultValue
                return acc
            }, {})
        ])
    }

    function handleRemove(idx) {
        return () => onChange(value.filter((_, valueIdx) => idx != valueIdx))
    }

    return (
        <>
            {value.map((childForm, idx) =>
                wrap(
                    <Form
                        isRoot={false}
                        key={`form-${idx}`}
                        values={childForm}
                        onChange={values => onChange([...value.slice(0, idx), values, ...value.slice(idx + 1)])}
                        errors={typeof error == 'string' ? null : error}
                        findError={(errors, childName) => parentErrors.errors && parentErrors.errors[`${name}.${idx}.${childName}`]}
                    >
                        {horizontal
                            ? (
                                <Row key={`row-${idx}`} type="flex" align="bottom" gutter={16}>
                                    {defaultFields.map((field, fieldIdx) =>
                                        <Col key={`fields-${fieldIdx}`} style={{ flex: 1 }}>
                                            {renderChildField(field, idx, fieldIdx)}
                                        </Col>
                                    )}
                                    {canManipulate && <Col key={`button-${idx}`}>
                                        <AntdForm.Item>
                                            <Button type="danger" icon="delete" onClick={handleRemove(idx)}>Удалить</Button>
                                        </AntdForm.Item>
                                    </Col>}
                                </Row>
                            )
                            : defaultFields.map((field, fieldIdx) => renderChildField(field, idx, fieldIdx))
                        }
                    </Form>, idx)
            )}
            {limit && value.length >= limit ||
            <>
                {canManipulate && <Button icon="plus" onClick={handleAdd}>Добавить</Button>}
            </>
            }
        </>
    )
}
