import { Button, Card, Col, Form as AntdForm, Row } from 'antd'
import { Field, Form, useErrors } from './Form'
import React from 'react'

export default function MorphOne ({ error, value, name, label, onChange, defaultFields }) {
    const parentErrors = useErrors()

    function renderField (field, fieldIdx) {
        return (
            <Field
                key={fieldIdx}
                name={field.attribute}
                type={field.type}
                label={field.label}
                isRequired={field.isRequired}
                readonly={field.readonly}
                {...field.meta}
            />
        )
    }

    return (
        <>
            <Form
                values={value}
                onChange={values => onChange(values)}
                errors={typeof error == 'string' ? null : error}
                findError={(errors, fieldName) => parentErrors.errors && parentErrors.errors[`${name}.${fieldName}`]}
            >
                {defaultFields.map((field, fieldIdx) => renderField(field, fieldIdx))}
            </Form>
        </>
    )
}
