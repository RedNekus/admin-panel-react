import React from 'react'
import { Field, Form } from "./Form"

export default function BelongsTo({ error, value, label, onChange, fields }) {
    return (
        <Field
            name={fields.attribute}
            type={fields.type}
            label={label}
            value={value}
            {...fields.meta}
        />
    )
}
