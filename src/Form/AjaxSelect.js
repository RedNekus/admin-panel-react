import { Select as AntdSelect } from 'antd'
import React, {useRef, useState} from 'react'
import { useApi } from '../Api/Api'

export default function Select ({ value, onChange, label, readonly, placeholder: initialPlaceholder, url, defaultOptions, withPreloaded, style, multiple = false }) {
    const placeholder = initialPlaceholder
        ? initialPlaceholder
        : '- Выберите значение -'
    const [options, setOptions] = useState(defaultOptions)
    const firstRun = useRef(true)
    const api = useApi()

    if (withPreloaded && firstRun.current) {
        getPreloaded()
        firstRun.current = false
    }

    async function getPreloaded()
    {
        const preloadedOptions = await api.get(url, {query: value})
        setOptions(preloadedOptions)
    }

    const handleSearch = (() => {
        let timeout = null

        return (query) => {
            clearTimeout(timeout)

            timeout = setTimeout(async () => {
                const options = await api.get(url, { query })
                setOptions(options)
            }, 500)
        }
    })()

    if (value) {
        value = isNaN(value) ? value : Number(value)
    }

    function renderOption (option, idx) {
        if (option.is_group) {
            return <AntdSelect.OptGroup
                key={idx}
                label={option.label}
            >
                {option.options.map((option, childIdx) =>
                    renderOption(option, childIdx),
                )}
            </AntdSelect.OptGroup>
        }

        return <AntdSelect.Option
            key={idx}
            value={option.value}
            title={option.label}
        >
            {option.label}
        </AntdSelect.Option>
    }

    return (
        <AntdSelect
            mode={multiple ? 'multiple' : 'default'}
            showSearch
            style={style}
            placeholder={placeholder}
            value={value || undefined}
            disabled={readonly}
            onChange={onChange}
            onSearch={handleSearch}
            label={label}
            filterOption={false}
            defaultActiveFirstOption={false}
        >
            {options.map((option, idx) =>
                renderOption(option, idx),
            )}
        </AntdSelect>
    )
}
