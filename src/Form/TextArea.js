import { Input } from "antd"
import React from 'react'

export default function TextArea({ value, onChange, label, readonly, placeholder, rows = 4 }) {
    return (
        <Input.TextArea
            rows={rows}
            label={label}
            placeholder={placeholder || label}
            value={value}
            onChange={e => onChange(e.target.value)}
            readOnly={readonly}
        />
    )
}
