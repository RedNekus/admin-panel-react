import { Button, Card, Col, notification, Row } from 'antd'
import React from 'react'
import { useApi } from '../Api/Api'
import { FieldsForm } from '../Form/Form'
import { useRouter } from '../Router/Router'
import { useSharedData } from '../SharedData/SharedData'

export default function LoginPage ({ errors }) {
    const fields = [
        {
            attribute: 'email',
            canOrder: true,
            defaultValue: '',
            displayValue: '',
            label: 'E-Mail',
            placeholder: 'E-Mail',
            type: 'text',
            value: '',
        },
        {
            attribute: 'password',
            canOrder: true,
            defaultValue: '',
            displayValue: '',
            label: 'Пароль',
            placeholder: 'Пароль',
            type: 'password',
            value: '',
        },
    ]

    const api = useApi()
    const router = useRouter()
    const { config } = useSharedData()

    async function onSubmit (values) {
        await api.post('/admin/login', values)
        notification.success({message: 'Вы успешно авторизованы'})
        router.push('/admin');
    }

    return (
        <Row type='flex' align='middle' justify='center' className='h-100'>
            <Col xs={20} sm={16} md={10} lg={8} xl={8}>
                <Row type='flex' justify='center'>
                    <Col>
                        <img
                            className='login-logo'
                            src={config.logo.dark}
                            alt={config.title}
                        />
                    </Col>
                </Row>
                <Row type='flex' justify='center'>
                    <Col span={24}>
                        <Card>
                            <FieldsForm fields={fields} onSubmit={onSubmit}>
                                {({ fields, isSubmitting }) => (
                                    <>
                                        {fields.elements}
                                        <Row gutter={[16, 16]}>
                                            <Col>
                                                <Button type='primary'
                                                        icon='login'
                                                        block
                                                        loading={isSubmitting}
                                                        htmlType='submit'>
                                                    Войти
                                                </Button>
                                            </Col>
                                        </Row>
                                    </>
                                )}
                            </FieldsForm>
                        </Card>
                    </Col>
                </Row>
            </Col>
        </Row>
    )
}
