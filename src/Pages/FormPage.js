import { Row, Col, Button, PageHeader, notification } from 'antd'
import React from 'react'

import { useApi } from '../Api/Api'
import { FieldsForm } from '../Form/Form'
import { Link, useRouter } from '../Router/Router'

export default function FormPage({ title, fields, submitUrl, links }) {
    const api = useApi()
    const router = useRouter()

    async function onSubmit(values) {
        await api.post(submitUrl, values)
        notification.success({ message: 'Данные успешно сохранены!' })
    }

    return (
        <PageHeader
            title={title}
            onBack={() => router.push(links.index)}
        >
            <FieldsForm fields={fields} onSubmit={onSubmit}>
                {({ fields, isSubmitting }) => (
                    <>
                        {fields.elements}
                        <Row gutter={[16, 16]} type='flex' justify='end'>
                            {links.actions && links.actions.length > 0 && links.actions.map(link => (
                                <Col>
                                    <a href={link.to} target={link.target} style={
                                        link.icon ? {marginLeft: '6px'} : {}
                                    }>
                                        <Button icon={link.icon}>

                                            {link.title}
                                        </Button>
                                    </a>
                                </Col>
                            ))}
                            {submitUrl &&
                            <Col>
                                <Button icon='save' type='primary' loading={isSubmitting} htmlType='submit'>
                                    Сохранить
                                </Button>
                            </Col>
                            }
                            <Col>
                                <Button>
                                    <Link to={links.index}>
                                        Назад
                                    </Link>
                                </Button>
                            </Col>
                        </Row>
                    </>
                )}
            </FieldsForm>
        </PageHeader>
    )
}
