import React, { useEffect, useRef, useState } from 'react'
import SortableTree from 'react-sortable-tree'
import 'react-sortable-tree/style.css' // This only needs to be imported once in your app
import {
    Avatar,
    Button,
    Col,
    Drawer,
    Icon,
    notification,
    PageHeader,
    Popconfirm,
    Row,
    Table,
    Tag,
    Tooltip,
} from 'antd'
import { red } from '@ant-design/colors'
import { useApi } from '../Api/Api'
import { Link, useRouter } from '../Router/Router'
import { FieldsForm, useFields } from '../Form/Form'
import qs from 'qs'
import moment from 'moment'

const DEFAULT_PER_PAGE = 15

export default function IndexPage ({ resource, paginator: initialPaginator }) {
    const router = useRouter()
    const api = useApi()
    const { filters: defaultFiltersValues } = router.query
    const [paginator, setPaginator] = useState(initialPaginator)
    const labels = resource.labels
    const defaultOrder = resource.defaultOrder
    const filters = useFields(resource.filters, defaultFiltersValues)
    const [filterDrawerState, setFilterDrawerState] = useState(false)
    const [sortableDrawerState, setSortableDrawerState] = useState(false)
    const [sortable, setSortable] = useState(null)
    const [searchQuery, setSearchQuery] = useState({
        ...router.query,
        orders: router.query.orders || defaultOrder,
        perPage: Number(router.query.perPage) || DEFAULT_PER_PAGE,
        page: Number(router.query.page) || 1,
    })
    const firstRun = useRef(true)
    const dataColumns = resource.fields.map((field, idx) => ({
        title: field.label,
        sorter: field.canOrder,
        dataIndex: field.attribute,
        key: field.attribute,
        sortOrder: searchQuery.orders && searchQuery.orders.field ===
        field.attribute && field.canOrder
            ? searchQuery.orders.direction + 'end'
            : false,
        ...field.meta,
        render: prettyRender(field, idx),
    }))

    function prettyRender (field, fieldIdx) {
        let render

        switch (field.type) {
            case 'mediaImage':
                render = (text, record, idx) => {
                    if (!text) {
                        return <>Нет изображения</>
                    }
                    return <Avatar src={text.sm_preview} style={{
                        border: '2px solid #ffffff',
                    }}/>
                }
                break
            case 'mediaImages':
                render = (text, record, idx) => {
                    if (!text || !text.length) {
                        return <>Нет изображений</>
                    }
                    let visibleImages = text.slice(0, 3)
                    const moreCount = text.length - visibleImages.length
                    visibleImages = visibleImages.map((image, imageIdx) => (
                        <Avatar key={imageIdx} src={image.sm_preview} style={{
                            marginLeft: imageIdx === 0 ? 0 : '-0.8rem',
                            zIndex: visibleImages.length - imageIdx,
                            border: '2px solid #ffffff',
                        }}/>
                    ))

                    const moreCountComponent = <span key='moreCount' style={{
                        paddingLeft: '4px',
                    }}>+ {moreCount}</span>

                    return !!moreCount
                        ? [...visibleImages, moreCountComponent]
                        : visibleImages
                }
                break
            case 'boolean':
                render = (text, record, idx) => (
                    <>
                        {text !== null && text === true ? 'Да' : 'Нет'}
                    </>
                )
                break
            case 'color':
                render = (text, record, idx) => (
                    <Tooltip title={text}><Tag color={text} style={{
                        marginRight: 0,
                        width: 20,
                        height: 20,
                    }}/></Tooltip>
                )
                break
            default:
                render = (text, record, idx) => (<>{text}</>)
        }

        return (text, record, idx) => {
            let link = paginator.data[idx].field_links[fieldIdx]

            if (link && link.href) {
                return <a href={link.href} target={link.target}>{render(text,
                    record, idx)}</a>
            }

            return render(text, record, idx)
        }

    }

    const actionColumns = [
        {
            title: '',
            dataIndex: 'actions',
            width: 150,
            key: 'actions',
            render: (text, record, idx) => {
                const links = paginator.data[idx].links
                return (
                    <div className='text-right'>
                        {links.actions.length > 0 && links.actions.map((link) =>
                            (
                                <a href={link.to} target={link.target}>
                                    <Tooltip placement='top' title={link.title}
                                             mouseLeaveDelay={0}>
                                        <Button
                                            size='small'
                                            type='link'
                                        >
                                            <Icon
                                                style={iconStyle}
                                                type={link.icon}
                                            />
                                        </Button>
                                    </Tooltip>
                                </a>
                            ),
                        )}

                        {links.actions.length > 0 &&
                        (
                            <br/>
                        )
                        }

                        {paginator.data[idx].canBeUpdated && links.edit &&
                        <Link to={links.edit}>
                            <Tooltip placement='top' title='Редактировать'
                                     mouseLeaveDelay={0}>
                                <Button
                                    size='small'
                                    type='link'
                                >
                                    <Icon
                                        style={iconStyle}
                                        type='edit'
                                    />
                                </Button>
                            </Tooltip>
                        </Link>
                        }
                        {paginator.data[idx].canBeViewed && links.view &&
                        <Link to={links.view}>
                            <Tooltip placement='top' title='Просмотреть'
                                     mouseLeaveDelay={0}>
                                <Button
                                    size='small'
                                    type='link'
                                >
                                    <Icon
                                        style={iconStyle}
                                        type='eye'
                                    />
                                </Button>
                            </Tooltip>
                        </Link>
                        }
                        {paginator.data[idx].canBeDeleted && links.delete &&
                        <Tooltip placement='top' title='Удалить'
                                 mouseLeaveDelay={0}>
                            <Popconfirm
                                placement='left'
                                title='Действительно хотите удалить?'
                                onConfirm={() => onDeleteButton(
                                    links.delete)}
                            >
                                <Button
                                    size='small'
                                    type='link'
                                >
                                    <Icon
                                        style={iconStyle}
                                        type='delete'
                                        theme='twoTone'
                                        twoToneColor={red.primary}
                                    />
                                </Button>
                            </Popconfirm>
                        </Tooltip>
                        }
                    </div>
                )
            },
        }]

    const columns = [
        ...dataColumns,
        ...actionColumns,
    ]

    const data = paginator.data.map(({ values }) => values)

    useEffect(() => {
        if (firstRun.current) {
            firstRun.current = false
            return
        }
        fetchData()
        updateUrl()
    }, [searchQuery, sortableDrawerState])

    useEffect(() => {
        setPaginator(initialPaginator)
    }, [initialPaginator])

    useEffect(() => {
        setSortable(resource.sortable.collection)
    }, [resource.sortable.collection])

    async function fetchData () {
        const response = await api.get(resource.links.index, searchQuery)
        setSortable(response.props.resource.sortable.collection)
        setPaginator(response.props.paginator)
    }

    function updateUrl () {
        const query = {
            filters: searchQuery.filters,
            orders: searchQuery.orders,
        }

        if (searchQuery.perPage !== DEFAULT_PER_PAGE) {
            query.perPage = searchQuery.perPage
        }
        if (searchQuery.page !== 1) {
            query.page = searchQuery.page
        }

        const search = qs.stringify(query)
        const url = Object.keys(search).length > 0
            ? `${router.location}?${search}`
            : router.location

        router.replace(url)
    }

    function handleTableChange (pagination, filters, sorter) {
        if (sorter.order) {
            setSearchQuery({
                ...searchQuery,
                page: pagination.current,
                perPage: pagination.pageSize,
                orders: {
                    field: sorter.columnKey,
                    direction: sorter.order === 'descend' ? 'desc' : 'asc',
                },
            })
        } else {
            const { ['orders']: _, ...newSearchQuery } = searchQuery
            setSearchQuery({
                ...newSearchQuery,
                perPage: pagination.pageSize,
                page: pagination.current,
            })
        }
    }

    async function onDeleteButton (url) {
        await api.post(url)
        fetchData()
        notification.success({ message: 'Успешно удалено!' })
    }

    function onSubmitFilters () {
        setSearchQuery({
            ...searchQuery,
            page: 1,
            filters: filters.filledValues,
        })
        setFilterDrawerState(false)
    }

    function onClearFilters () {
        setSearchQuery({
            page: 1,
            perPage: searchQuery.perPage,
            orders: searchQuery.orders,
        })
        filters.clear()
        setFilterDrawerState(false)
    }

    function onToggleFilterDrawer () {
        setFilterDrawerState(!filterDrawerState)
    }

    function onToggleSortableDrawer () {
        setSortableDrawerState(!sortableDrawerState)
    }

    function onChipClose (key) {
        const { [key]: _, ...newValues } = searchQuery.filters

        setSearchQuery({
            ...searchQuery,
            page: 1,
            perPage: searchQuery.perPage,
            filters: newValues,
        })
        filters.remove(key)
    }

    function searchValueLabels (options, value) {
        options = flatOptGroups(options)
        if (Array.isArray(value)) {
            return options.reduce((acc, item) => {
                if (value.includes(item.value)) {
                    acc.push(item.label)
                }
                return acc
            }, []).join(', ')
        }

        let selectedOption = options.find(item => item.value == value)
        return selectedOption ? selectedOption.label : value
    }

    function flatOptGroups (options) {
        return options.flatMap(function (value) {
            return value.is_group
                ? flatOptGroups(value.options)
                : value
        })
    }

    function prettyChipsValue (value, props) {
        if (props.momentFormat) {
            return moment(value).format(props.momentFormat)
        }

        if (props.defaultOptions && props.defaultOptions.length > 0) {
            return searchValueLabels(props.defaultOptions, value)
        }

        if (props.options && props.options.length > 0) {
            return searchValueLabels(props.options, value)
        }

        return value
    }

    const extraButtons = [
        (resource.links.create &&
            <Link key='create' to={resource.links.create}>
                <Tooltip placement='top' title={labels.createTitle}>
                    <Button
                        shape='circle'
                        type='primary'
                        icon='plus'
                    >
                    </Button>
                </Tooltip>
            </Link>

        ),
        (resource.filters.length > 0 &&
            <Tooltip key='filter' placement='top' title='Фильтр'>
                <Button
                    onClick={onToggleFilterDrawer}
                    shape='circle'
                    type='primary'
                    icon='filter'
                />
            </Tooltip>
        ),
        (sortable &&
            <Tooltip key='sortable' placement='top' title='Сортировка'>
                <Button
                    onClick={onToggleSortableDrawer}
                    shape='circle'
                    type='primary'
                    icon='sort-ascending'
                />
            </Tooltip>
        ),
    ]

    function reorder (list, startIndex, endIndex) {
        let result = Array.from(list)
        const [removed] = result.splice(startIndex, 1)
        result.splice(endIndex, 0, removed)

        return result
    }

    function onSortEnd (result) {
        setSortable(result)

        api.post(resource.sortable.url, result)
    }

    function canDrop (item) {
        if (!resource.sortable.rule) {
            return true
        }

        if (resource.sortable.rule === 'rule_within_the_parent') {
            if (!item.nextParent && item.node.level === 0) {
                return true
            }

            if (!item.prevParent) {
                return false
            }

            if (item.nextParent &&
                (item.nextParent.id === item.prevParent.id) &&
                (item.nextParent.level + 1 === item.node.level)) {
                return true
            }
        }

        if (resource.sortable.rule === 'rule_within_the_level') {
            if (!item.nextParent && item.node.level === 0) {
                return true
            }

            if (item.nextParent &&
                (item.nextParent.level + 1 === item.node.level)) {
                return true
            }
        }

        return false
    }

    return (
        <PageHeader
            title={labels.indexTitle}
            extra={extraButtons}
        >
            {resource.filters.length > 0 &&
            <>
                <Drawer
                    width='30%'
                    title='Фильтры'
                    onClose={onToggleFilterDrawer}
                    visible={filterDrawerState}
                >
                    <FieldsForm
                        fields={resource.filters}
                        values={filters.values}
                        onChange={filters.setValues}
                        onSubmit={onSubmitFilters}
                    >
                        {() => (
                            <>
                                <Row gutter={[16, 16]}>
                                    {filters.elements.map((field, key) => (
                                        <Col span={24} key={key}>
                                            {field}
                                        </Col>
                                    ))}
                                </Row>
                                <Row gutter={[16, 16]}>
                                    <Col span={24}>
                                        <Button htmlType='submit'
                                                type='primary'
                                                icon='filter'
                                        >
                                            Применить
                                        </Button>
                                    </Col>
                                    <Col span={24}>
                                        <Button icon='close'
                                                onClick={onClearFilters}
                                        >
                                            Сбросить
                                        </Button>
                                    </Col>
                                </Row>
                            </>
                        )}
                    </FieldsForm>
                </Drawer>
                {searchQuery.filters &&
                Object.keys(searchQuery.filters).length > 0 && (
                    <Row gutter={[16, 16]}>
                        <Col span={24}>
                            {Object.keys(searchQuery.filters).map((key) =>
                                (searchQuery.filters[key] && (
                                        <Tag key={key} color='blue' closable
                                             onClose={() => onChipClose(
                                                 key)}>
                                            {filters.elements[key].props.label}: {prettyChipsValue(
                                            searchQuery.filters[key],
                                            filters.elements[key].props)}
                                        </Tag>
                                    )
                                ),
                            )}
                        </Col>
                    </Row>
                )}
            </>
            }
            {sortable &&
            <>
                <Drawer
                    width='70%'
                    title='Сортировка'
                    onClose={onToggleSortableDrawer}
                    visible={sortableDrawerState}
                >
                    <div style={{ height: '100vh' }}>
                        <SortableTree
                            treeData={sortable}
                            onChange={onSortEnd}
                            canDrop={canDrop}
                        />
                    </div>
                </Drawer>
            </>
            }
            <Table
                onChange={handleTableChange}
                columns={columns}
                dataSource={data}
                bordered={true}
                scroll={{ x: 0 }}
                rowKey={record => record.unique_id}
                pagination={{
                    locale: { items_per_page: '' },
                    showQuickJumper: true,
                    showSizeChanger: true,
                    pageSizeOptions: [
                        '10',
                        String(DEFAULT_PER_PAGE),
                        '20',
                        '30',
                        '50'],
                    pageSize: searchQuery.perPage,
                    current: searchQuery.page,
                    total: paginator.total,
                }}
            />
        </PageHeader>
    )
}

const iconStyle = {
    fontSize: '1.1rem',
}
