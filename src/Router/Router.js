import React, { useState, useEffect, useContext, useMemo, useRef } from 'react'
import qs from 'qs'

const RouterContext = React.createContext()

export default function Router({ children }) {
    const wentBack = useRef(true)
    const [location, setLocation] = useState(window.location.pathname)
    const query = useMemo(() => qs.parse(window.location.search.substr(1)), [location])

    useEffect(() => {
        window.onpopstate = e => {
            setLocation(window.location.pathname)
        }
    }, [])

    useEffect(() => {
        if (wentBack.current) {
            wentBack.current = false
            return
        }
        window.history.pushState({}, '', location)
    }, [location])

    function push(url) {
        setLocation(url)
    }

    function replace(url) {
        window.history.replaceState({}, '', url)
    }

    function back() {
        wentBack.current = true
        window.history.back()
    }

    return (
        <RouterContext.Provider value={{ location, push, back, query, replace }}>
            {children}
        </RouterContext.Provider>
    )
}

export function Link({ children, back, to, callback }) {
    const router = useRouter()

    if (!callback) {
        callback = e => {
            e.preventDefault()

            if (back) {
                router.back()
            } else {
                router.push(to)
            }
        }
    }

    return (
        <a href={to} onClick={callback}>
            {children}
        </a>
    )
}

export function useRouter() {
    return useContext(RouterContext)
}
