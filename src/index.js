import AdminRenderer from './AdminRenderer'
import registry from './Registry/Registry'

export default AdminRenderer
export { registry }
